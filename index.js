// Using DOM
// Retrieve an element from webpage
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

txtFirstName.addEventListener("keyup", myFunction);
txtLastName.addEventListener("keyup", myFunction);

function myFunction() {
	spanFullName.innerHTML = txtFirstName.value + " " +txtLastName.value;
}

/*txtFirstName.addEventListener('keyup', (event) => {
	// Contains the element where the event happened
	console.log(event.target);
	// Gets the value of the input object
	console.log(event.target.value);
});*/
